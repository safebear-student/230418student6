package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {

    @Test
    public void testLogin() {
        //Step 1 Confirm we're on the Welcome Page
        assertTrue(welcomePage.checkCorrectPage());
        //Step 2 click the Login link and the Login Page loads
        welcomePage.clickOnLogin();

        // Step 3 confirm that we're now on the Login page
        assertTrue(loginPage.checkCorrectPage());

        // Step 4 login to the app

        loginPage.login("testuser", "testing");

        // Step 5. check we're on the User Page
        assertTrue(userPage.checkCorrectPage());

        // Step 6. logout
        userPage.clickOnLogout();

        // Step 7 check we're now on the welcome page
        assertTrue(welcomePage.checkCorrectPage());

    }


}

