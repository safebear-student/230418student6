package com.safebear.app.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    WebDriver driver;

    @FindBy(id = "myid")
    WebElement username_field;

    @FindBy(id = "mypass")
    WebElement password_field;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Sign In");
    }

    public void login(String username, String password){

        username_field.sendKeys(username);
        password_field.sendKeys(password);
        password_field.submit();


    }
}