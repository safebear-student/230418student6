package com.safebear.app.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WelcomePage {

    @FindBy(linkText = "Login")
    WebElement loginLink;
    WebDriver driver;

    WelcomePage welcomePage;
    LoginPage loginPage;

    public WelcomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    public void clickOnLogin() {
        loginLink.click();
    }

    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Welcome");
    }
}
