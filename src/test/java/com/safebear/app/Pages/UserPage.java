package com.safebear.app.Pages;

import com.safebear.app.Pages.LoginPage;
import org.apache.commons.exec.LogOutputStream;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserPage {

    @FindBy(linkText = "Logout")
    WebElement logoutLink;

    WebDriver driver;

    public UserPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    public void clickOnLogout() {
        logoutLink.click();
    }

    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("logout");
    }
}